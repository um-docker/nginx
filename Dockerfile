FROM alpine:latest

MAINTAINER Aleksandr Malov "masashama@gmail.com"

RUN apk add --no-cache nginx \
    && rm -rf /etc/nginx/conf.d \
    && mkdir -p /srv/main \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/nginx.conf
COPY index.html /srv/main

EXPOSE 80

STOPSIGNAL SIGTERM

CMD nginx -g "daemon off; pid /tmp/nginx.pid;"